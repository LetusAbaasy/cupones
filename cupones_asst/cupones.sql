-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2017 at 02:03 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cupones`
--

-- --------------------------------------------------------

--
-- Table structure for table `cupones`
--

CREATE TABLE `cupones` (
  `id_cupones` int(11) NOT NULL,
  `serial` varchar(1000) NOT NULL,
  `status` varchar(1000) NOT NULL,
  `user_id` varchar(1000) NOT NULL,
  `amigo` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cupones`
--

INSERT INTO `cupones` (`id_cupones`, `serial`, `status`, `user_id`, `amigo`) VALUES
(1, '95jbrTDh', 'Pendiente', '10211698571913074', 'Andrey Rodriguez'),
(2, 'RB88aJ0F', 'Pendiente', '10211698571913074', NULL),
(3, 'P8IZ156a', 'Pendiente', '10211698571913074', 'Jonathan Cruz'),
(4, 'joS4dxzi', 'Pendiente', '10211698571913074', '');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(11) NOT NULL,
  `user_id` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`payment_id`, `user_id`, `product_id`, `txn_id`, `payment_gross`, `currency_code`, `payer_email`, `payment_status`) VALUES
(1, '10211698571913074', 2, '61V26119D8508241X', 20.00, ' USD', 'sermat123@hotmail.com', 'Pendiente'),
(2, '10211698571913074', 4, '4L7504358L657111P', 13.00, ' USD', 'sermat123@hotmail.com', 'Pendiente'),
(3, '10211698571913074', 8, '9WP17792FP421762C', 20.00, ' USD', 'sermat123@hotmail.com', 'Pendiente'),
(4, '10211698571913074', 7, '83U51284BT648101U', 65.00, ' USD', 'sermat123@hotmail.com', 'Pendiente');

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `precio` float(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `imagen`, `precio`, `status`, `descripcion`) VALUES
(1, 'Brahma', '1.jpg', 35.00, 1, 'Esta es una brahma'),
(2, 'Aquiles', '2.jpg', 20.00, 1, 'Esta es una aquiles'),
(3, 'Bosi', '3.jpg', 55.00, 1, 'Esta es una bosi'),
(4, 'Nike', '6.png', 13.00, 1, 'Esto es unos nike'),
(5, 'Addidas', 'producto_gLQzsEKwhFLogo_brand_Adidas.png', 0.00, 1, 'Son addidas'),
(6, 'Rebook', 'producto_DuI61cGJbuLogotipo-REEBOK-png-3.png', 45.00, 1, 'Son unas rebook'),
(7, 'New Balance', 'producto_K9HCRMvpfJ0323746001489532240.png', 65.00, 1, 'Son unas new balance'),
(8, 'Westland', 'producto_5KM4GTpGHibanner-westland.png', 20.00, 1, 'Son unas westland');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture_url`, `profile_url`, `created`, `modified`) VALUES
(1, 'facebook', '10211698571913074', 'Sergio', 'Martínez', 'sermat123@hotmail.com', 'male', 'es_LA', 'https://scontent.xx.fbcdn.net/v/t1.0-1/c0.4.50.50/p50x50/22046940_10211587401813891_5217995231063766156_n.jpg?oh=05b0bf926f9b9b7ea8b19c4613bbc6e6&oe=5AAD50D9', 'https://www.facebook.com/10211698571913074', '2017-10-12 14:33:00', '2017-11-14 14:03:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cupones`
--
ALTER TABLE `cupones`
  ADD PRIMARY KEY (`id_cupones`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cupones`
--
ALTER TABLE `cupones`
  MODIFY `id_cupones` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
