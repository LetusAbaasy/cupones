<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal extends CI_Controller 
{
	 function  __construct(){
		parent::__construct();
		$this->load->library('facebook');
		$this->load->library('paypal_lib');
		$this->load->model('ProductosM');
	 }
	 
	 function success(){
	 	if($this->facebook->is_authenticated()){
			$userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture,likes,friends');
	 		$userData['email'] = $userProfile['email'];
	 		$userData['friends'] = $userProfile['friends']['data'];
            $data['userData'] = $userData;
	 	}
	    //get the transaction data
		$paypalInfo = $this->input->get();
		$data['title'] = 'Compra realizada';
		  
		$data['item_number'] = $paypalInfo['item_number']; 
		$data['txn_id'] = $paypalInfo["tx"];
		$data['payment_amt'] = $paypalInfo["amt"];
		$data['currency_code'] = $paypalInfo["cc"];
		$data['status'] = $paypalInfo["st"];

		//pass the transaction data to view
		$this->load->view('include/header', $data);
        $this->load->view('paypal/success', $data);
		$this->load->view('include/footer');

	 }
	 
	 function cancel(){
		$data['title'] = 'Cancelación de compra';

		$this->load->view('include/header', $data);
        $this->load->view('paypal/cancel');
		$this->load->view('include/footer');

	 }
	 
	 function ipn(){
		//paypal return transaction details array
		$paypalInfo	= $this->input->post();

		$data['user_id'] = $paypalInfo['custom'];
		$data['product_id']	= $paypalInfo["item_number"];
		$data['txn_id']	= $paypalInfo["txn_id"];
		$data['payment_gross'] = $paypalInfo["payment_gross"];
		$data['currency_code'] = $paypalInfo["mc_currency"];
		$data['payer_email'] = $paypalInfo["payer_email"];
		$data['payment_status']	= $paypalInfo["payment_status"];

		$paypalURL = $this->paypal_lib->paypal_url;	
		$result	= $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
		
		//check whether the payment is verified
		if(preg_match("/VERIFIED/i",$result)){
		    //insert the transaction data into the database
			$this->ProductosM->insertTransaction($data);
		}
    }
}