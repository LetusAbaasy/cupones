<div id="carousel1" class="carousel slide">
    <ol class="carousel-indicators">
      <li data-target="#carousel1" data-slide-to="0" class="active"></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active"> 
        <img class="img-responsive" src="assets/img/slider/2.jpg" alt="thumb">
        <div class="carousel-caption">Cupones Minuto</div>
      </div>
    </div>
    <a class="left carousel-control" href="#carousel1" data-slide="prev"><span class="icon-prev"></span></a> <a class="right carousel-control" href="#carousel1" data-slide="next"><span class="icon-next"></span></a>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="clearfix"></div>
        <hr/>
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h3>¿Quienes Somos?</h3>
                    <p>Cupones minuto es una solución tecnológica que permite a un usuario realizar compras de cupones de regalo online para ser redimidos en tiendas de zapatos aliadas.
                    “Para la construcción de la solución se usa la metodología de desarrollo ágil “Scrum”, la cual se adapta a las necesidades del proyecto, pues, es un proceso en el que se aplican un conjunto de buenas prácticas para trabajar colaborativamente, en equipo, y obtener el mejor resultado posible de un proyecto.”
                    </p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="container">
            <div class="row">
                <div id="easyPaginate" class="col-md-12">
                    <h3 class="text-center">Cupones</h3>
                    <?php if(!empty($productos)): foreach($productos as $producto): ?>
                    <div class="cont col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img class="img-thumbnail img-responsive" src="<?php echo base_url().'assets/img/productos/'.$producto['imagen']; ?>" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$<?php echo $producto['precio']; ?> USD</h4>
                                <h4><a href="javascript:void(0);"><?php echo $producto['nombre']; ?></a></h4>
                                <p><?php echo $producto['descripcion']; ?></p>
                            </div>
                            <div class="ratings">
                                <?php if($fb != true){ ?>
                                <a href="<?php echo base_url().'productos/buy/'.$producto['id']; ?>"><img src="<?php echo base_url(); ?>assets/img/x-click-but01.gif" style="width: 70px;"></a>
                                <?php } ?>
                                <p class="pull-right">1 Vistas</p>
                                <div id="rateYo<?php echo $producto['id']; ?>"></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<hr/>
<h3 class="text-center">Nuestros Aliados</h3>
<div id="carousel2" class="carousel slide">
    <ol class="carousel-indicators">
      <li data-target="#carousel2" data-slide-to="0" class="active"></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active"> 
        <img class="img-responsive" src="assets/img/slider/3.png" alt="thumb">
        <div class="carousel-caption"></div>
      </div>
    </div>
    <a class="left carousel-control" href="#carousel2" data-slide="prev"><span class="icon-prev"></span></a> <a class="right carousel-control" href="#carousel2" data-slide="next"><span class="icon-next"></span></a>
</div>
<div class="clearfix"></div>
