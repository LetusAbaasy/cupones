<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="nombreProducto">Nombre:*</label>
				<input type="text" id="nombreProducto" name="nombreProducto" class="form-control" placeholder="Nombre del producto" required="">
			</div>
			<div class="form-group">
				<label for="precioProducto">Precio:*</label>
				<input type="number" id="precioProducto" name="precioProducto" class="form-control" placeholder="Precio" required="">
			</div>
			<div class="form-group">
				<label for="descripcionProducto">Descripción:*</label>
				<textarea id="descripcionProducto" class="form-control" placeholder="Descripción..."></textarea>
			</div>
			<div class="form-group">
				<label for="imagenProducto">Imagen:*</label>
				<input type="file" id="imagenProducto" name="imagenProducto" class="form-control">
			</div>
			<button class="btn btn-success btn-block" id="crearProducto">Crear</button>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<hr/>
<div class="col-md-12">
	<h3>Productos en venta</h3>
	<table id="tablaProductos" width="100%" border=0 class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Precio</td>
                <td>Descripción</td>
                <td>Acción</td>
            </tr>
        </thead>
        <tbody id="tbody">
        	<?php
                foreach ($productos as $producto) {
	                echo "<tr><td>$producto->nombre</td>";
	                echo "<td>$producto->precio</td>";
	                echo "<td>$producto->descripcion</td>";
	                echo "<td><button class='btn btn-warning' data-id='$producto->id' title=''><i class='fa fa-file' aria-hidden='true'></i></button></td></tr>";
	            }
            ?>
        </tbody>
    </table>
</div>
<div class="clearfix"></div>
