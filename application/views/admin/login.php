<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3>Iniciar Sesión</h3>
			<div class="form-group">
				<label for="usuario"><i class="fa fa-user" aria-hidden="true"></i> Nombre de usuario:</label>
				<input type="text" name="usuario" class="form-control" id="usuario" placeholder="Nombre de usuario" required="" size="10">
			</div>
			<div class="form-group">
				<label for="password"><i class="fa fa-key" aria-hidden="true"></i> Contraseña:</label>
				<input type="password" name="password" class="form-control" id="password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;" required="" size="10" autocomplete="off">
			</div>
			<button class="btn btn-success btn-block">Iniciar Sesión</button>
		</div>
	</div>
</div>