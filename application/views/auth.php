<div class="container-fluid">
	<div class="row-fluid">
		<div class=" center-block">
			<?php
			if(!empty($authUrl)) { ?>
			    <div class="main text-center">
			      	<h3>Inicia sesión con Facebook<span class="form_login">, ó <a href="#">crea una cuenta.</a></span></h3>
			      	<div class="row">
			        	<div class="col-md-12">
			          		<a href="<?php echo $authUrl; ?>" class="btn btn-lg btn-primary btn-block"><i class="fa fa-facebook" aria-hidden="true"></i> Iniciar con Facebook</a>
			        	</div>
			      	</div>
			      	<div class="form_login">
				      	<div class="login-or">
				        	<hr class="hr-or">
				        	<span class="span-or">ó</span>
				      	</div>
				      	<form role="form">
					        <div class="form-group">
					          <label for="">Usuario:</label>
					          <input type="text" class="form-control" id="" placeholder="Nombre de usuario">
					        </div>
					        <div class="form-group">
					          <label for="">Contraseña:</label>
					          <input type="password" class="form-control" id="" placeholder="Contraseña">
					        </div>
					        <button class="btn btn btn-success">Iniciar Sesión</button>
					        <div class="clearfix"></div>
					        <hr/>
					        <div class="col-md-12">
								<div class="fb-like" data-share="true" data-show-faces="true"></div>
							</div>
				      	</form>
				      </div>
			    </div>
				<?php }else{ ?>
		    	<div class="">
		    		<button class="btn btn-danger pull-right" id="salirFacebook">Cerrar Sesión</button>
		    		<h3>Perfil</h3>
		    		<div class="col-md-4">
						<img class="img-thumbnail img-responsive" src="<?php echo $userData['picture_url']; ?>" alt="Imagen del Usuario" width="50" height="50"/>
						<h4>Nombre y Apellido:</h4>
						<label><?php echo $userData['first_name'].' '.$userData['last_name']; ?></label>
						<h4>Correo:</h4>
						<label><?php echo $userData['email']; ?></label>
					</div>
					<div class="col-md-4">
						<h4>Lista de amigos que les gusta Cupones Minuto:</h4>
						<?php
							for ($i=0; $i < count($userData['friends']); $i++) { 
								echo "<a target='blank_' href=http://www.facebook.com/".$userData['friends'][$i]['id'].">".$userData['friends'][$i]['name']."</a> - ";
							}
					 	?>
				    </div>
				    <?php if(count($userData['likes']) > 0){ ?>
				    <div class="col-md-4">
				    	<h4>Lista de tus "Me Gusta":</h4>
						<?php 
							for ($i=0; $i < count($userData['likes']); $i++) { 
								echo "<a target='blank_' href='http://www.facebook.com/".$userData['likes'][$i]['id']."'>".$userData['likes'][$i]['name']."</a> - ";
							}
					 	?>
				    </div>
				    <div class="clearfix"></div>
				    <hr/>
				    <?php } ?>
				    <div id="" class="">
				    	<h4>Historial de Cupones Comprados:</h4>
                        <div class="ui link cards">
				    	<?php
				    		foreach ($cupones as $cupon) {
				    		    $amigo = $cupon->amigo;
                                echo '<div class="card">';
                                    echo '<div class="image">';
                                        if($amigo != null || $amigo != ""){
                                            echo '<img class="ui wireframe image" src="'.base_url('assets/img/fbb.png').'">';
                                        }else{
                                            echo '<img class="ui wireframe image" src="'.base_url('assets/img/image.png').'">';
                                        }
                                    echo '</div>';
                                    echo '<div class="content">';
                                        echo "<label>Serial:</label> <p/>".$cupon->serial."</p>";
                                        echo "<label>Estado:</label> <p>".$cupon->status."</p>";
                                        if($amigo != null || $amigo != ""){
                                            echo "<label>Amigo:</label><p>".$amigo."</p>";
                                        }else{
                                            echo "<label>Amigo:</label><p>No fue seleccionado</p>";
                                        }
                                    echo '</div>';
				    			echo "</div>";
				    		}
				    	?>
				    	</div>
				    </div>
                    <div class="clearfix"></div>
                    <hr/>
				    <div id="" class="">
				    	<h4>Historial de Compras:</h4>
                        <div class="ui link cards">
				    	<?php 
				    		foreach ($compras as $compra) {
				    			echo '<div class="card">';
                                    echo '<div class="image">';
                                      echo '<img class="ui wireframe image" src="'.base_url('assets/img/paypal.png').'">';
                                    echo '</div>';
                                echo '<div class="content">';
                                    echo "<label>ID:</label><p>".$compra->product_id."</p>";
                                    echo "<label>txn_id:</label><p>".$compra->txn_id."</p>";
                                    echo "<label>payment_gross:</label><p>".$compra->payment_gross."</p>";
                                    echo "<label>currency_code:</label><p>".$compra->currency_code."</p>";
                                    echo "<label>payer_email:</label><p>".$compra->payer_email."</p>";
                                    echo "<label>payment_status:</label><p>".$compra->payment_status."</p>";
				    			echo '</div>';
				    			echo "</div>";
				    		}
				    	?>
				    	</div>
				    </div>
				</div>
				<div class="hidden">
					<?php echo $userData['oauth_uid']; ?>
					<?php echo $userData['gender']; ?>
					<?php echo $userData['locale']; ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>