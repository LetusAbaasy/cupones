<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="refresh" content="7200" />
    <meta name="application-name" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="revisit-after" content="30 days" />
    <meta name="distribution" content="web" />
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW" />
    <link href="<?php echo base_url('assets/img/favicon.ico')?>" type="image/x-icon" rel="icon" />
    <meta name="theme-color" content="#000000"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="white-translucent" />
    <!-- Title -->
	<title>Cupones Minuto | <?php echo $title; ?></title>
</head>
<body class="nav-md">
    <header id="header">
        <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <div class="row-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo base_url(); ?>">Cupones Minuto</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/img/logo.jpg'); ?>" alt="Logo"></a> -->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav pull-right">
                  <li><a href="<?php echo base_url('administrador'); ?>">Administrador</a></li>
                  <li><a href="<?php echo base_url('registro'); ?>">Registro</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div><!--/.container-fluid -->
      </nav>
    </header>
    <hr/>
    <br/>