<a id="back-to-top" class="btn btn-default back-to-top" role="button" title="Volver arriba" data-toggle="tooltip" data-placement="left"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
<!--footer-->
<hr/>
<footer class="footer1">
	<div class="container-fluid">
		<div class="row-fluid"><!-- row -->
        	<!-- <div class="col-lg-3 col-md-3">
                <ul class="list-unstyled clear-margins">
                	<li class="widget-container widget_nav_menu">
                        <h1 class="title-widget">-</h1>
                        <ul>
                        	<li><a  href="#"><i class="fa fa-angle-double-right"></i> About Us</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i> Contact Us</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i> Success Stories</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i> PG Courses</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i> Achiever's Batch</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Regular Batch</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Test & Discussion</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Fast Track T & D</a></li>
                        </ul>
					</li>
                </ul>
        	</div>
            <div class="col-lg-3 col-md-3">
           	 	<ul class="list-unstyled clear-margins">
                	<li class="widget-container widget_nav_menu">
                        <h1 class="title-widget">-</h1>
                        <ul>
							<li><a  href="#"><i class="fa fa-angle-double-right"></i>  Test Series Schedule</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Postal Coaching</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  PG Dr. Bhatia Books</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  UG Courses</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Satellite Education</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Study Centres</a></li>
                            <li><a  href="#"><i class="fa fa-angle-double-right"></i>  State P.G. Mocks</a></li>
                            <li><a  href="#" target="_blank"><i class="fa fa-angle-double-right"></i> Results</a></li>
                        </ul>
					</li>
                </ul>       
            </div> -->
            <div class="col-lg-6 col-md-6"><!-- widgets column left -->
		            <ul class="list-unstyled clear-margins"><!-- widgets -->
                    	<li class="widget-container widget_nav_menu"><!-- widgets list -->
                        	<h1 class="title-widget">Comparte...</h1>
                        	<ul>
					            <li><div  class="fb-like" data-share="true" data-width="450" data-show-faces="true"></div></li>
								<!-- <li><a href="#"><i class="fa fa-angle-double-right"></i> Online Test Series</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> Grand Tests Series</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> Subject Wise Test Series</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> Smart Book</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i> Test Centres</a></li>
					            <li><a href="#"><i class="fa fa-angle-double-right"></i>  Admission Form</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right"></i>  Computer Live Test</a></li> -->
                            </ul>
						</li>
                    </ul>
            </div><!-- widgets column left end -->
            <div class="col-lg-6 col-md-6"><!-- widgets column center -->
            	<ul class="list-unstyled clear-margins"><!-- widgets -->
                	<li class="widget-container widget_recent_news"><!-- widgets list -->
                    <h1 class="title-widget title-widget2">Contacto</h1>
                		<div class="footerp"> 
                            <h2 class="title-median">Cupones Minuto.</h2>
                            <p>Correo Electrónico: <a href="mailto:marsarmy@outlook.com">marsarmy@outlook.com</a></p>
                            <p>Número de Teléfono:
						    <p>+57 313 267 9590</p>
                        </div>
                        <div class="social-icons">
                    		<ul class="nomargin">   
				                <a href="#"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
					            <a href="#"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
					            <a href="#"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
					            <a href="mailto:marsarmy@outlook.com"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>
							</ul>
                        </div>
            		</li>
              	</ul>
           	</div>
        </div>
	</div>
</footer>
<!--header-->
<div class="footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="copyright">
					© <?php echo date('Y'); ?>, Cupones Minuto, Todos los derechos reservados.
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="design">
					 <a href="<?php echo base_url(); ?>">Cupones Minuto</a> |  <a target="_blank" href="https://get.adobe.com/es/flashplayer/">Web Design & Desarrollo por Sergio Martínez y Equipo.</a>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="hidden" id="assets">
	    <!-- Styles -->
	    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
	    <!-- Custom CSS -->
	    <link href="<?php echo base_url('assets/css/styles.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/notifIt.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/bootstrap-select.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/animate.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/bootstrap-dropdownhover.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/jquery.rateyo.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/semantic.min.css')?>" rel="stylesheet" type="text/css" />
	    <!-- Scripts -->
	    <script src="<?php echo base_url('assets/js/modernizr.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/jquery-3.1.1.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/notifIt.js')?>" type="text/javascript" ></script>
	    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/jquery.validate.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/bootstrap-select.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/langs/selector-i18n/defaults-es_ES.js')?>" type="text/javascript"></script>
	        <!-- Data Tables -->
	    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/dataTables.buttons.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/dataTables.bootstrap.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.html5.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.print.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.flash.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.colVis.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/jszip.min.js')?>" type="text/javascript"></script>
	    <!--<script src="<?php echo base_url('assets/js/pdfmake.min.js')?>" type="text/javascript"></script>-->
	    <script src="<?php echo base_url('assets/js/vfs_fonts.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.bootstrap.min.js')?>" type="text/javascript"></script>
	        <!-- Fin Data Tables -->  
	    <script src="<?php echo base_url('assets/js/sidebar-menu.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/paging.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/bootstrap-dropdownhover.min.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/echarts.min.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/map/js/world.js')?>"></script>
  		<script src="<?php echo base_url('assets/js/jquery.easyPaginate.js')?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.rateyo.min.js')?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/semantic.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/script.js')?>" type="text/javascript"></script>
	    <script type="text/javascript">
		  window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1584590751598884',
		      xfbml      : true,
		      version    : 'v2.10'
		    });
		    FB.AppEvents.logPageView();
		  };

		  (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/en_US/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>
	</div>
</body>
</html>