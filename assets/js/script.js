$( document ).ready(function(){
    init();
    var hashUrl = window.location.hash;
    var appUrl = unescape(window.location.href);
    var url = unescape(window.location.href);
    var appUrl = appUrl.split('/');
    //var baseURL = appUrl[0]+'//'+appUrl[2]+'/';
    var baseURL = appUrl[0]+'//'+appUrl[2]+'/'+appUrl[3]+'/';
    var $funcion = appUrl[4];
    var funcion_ = appUrl[5];

    $('#easyPaginate').easyPaginate({
        paginateElement: 'div.cont',
        elementsPerPage: 6,
        effect: 'fade'
    });

    $('#h1').easyPaginate({
        paginateElement: 'div.cont',
        elementsPerPage: 6,
        effect: 'fade'
    });

    $('#h2').easyPaginate({
        paginateElement: 'div.cont',
        elementsPerPage: 6,
        effect: 'fade'
    });

    $("#crearProducto").click(function(){
        $nombreProducto = $("#nombreProducto").val();
        $precioProducto = $("#precioProducto").val();
        $descripcionProducto = $("#descripcionProducto").val();

        var file_data = $('#imagenProducto').prop('files')[0];   
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('nombreProducto', $nombreProducto);              
        form_data.append('precioProducto', $precioProducto);                 
        form_data.append('descripcionProducto', $descripcionProducto);

        $.ajax({
            url: baseURL+'administrador/crearProducto',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            dataType: "JSON",
            success: function(response){
                console.log(response);
                notificacion(response.msg, "success");
            },
            error: function(ev){
                //Do nothing
            }
         });
    });

    $("#salirFacebook").click(function(){
        redirect(baseURL+"logout");
    });

    for($j = 0; $j < 100; $j++){
        $("#rateYo"+$j).rateYo({
            numStars: 5,
            rating: 3,
            maxValue: 5,
            precision: 2,
            spacing: "1px",
            multiColor: {
              "startColor": "#FF0000", //RED
              "endColor"  : "#00FF00"  //GREEN
            }
        });
    }

    $('#amigosRegalar').selectpicker('refresh');

    $(".enviarCorreo").click(function(){
        $email = $(this).attr("data-email");
        $item_number = $("#item_number").html();
        $amigo = $("#amigosRegalar").val();
        $txn_id = $("#txn_id").html();
        $status = $("#status").html();

        $pago = $("#pago").html();
        $split = $pago.split('-');
        $payment_amt = $split[0];
        $currency_code = $split[1];
        

        data = {
            'email': $email,
            'amigo': $amigo,
            'item_number': $item_number,
            'txn_id': $txn_id,
            'status': $status,
            'payment_amt': $payment_amt,
            'currency_code': $currency_code
        }

        $.ajax({
            url: baseURL+"home/email",
            type: "post",
            dataType: "JSON",
            data: data,
            beforeSend: function(){ 
                notificacion("Espere...", "success");
            },
            success: function (response) {
                notificacion(response.msg, "success");
                setInterval("redirect('"+baseURL+"')", 4000);
            },
            error: function(ev){
                console.error(ev);
            }
        });
    });
    
});

function redirect(response){
    $url = response.replace('"','').replace('"','');
    $(window).attr("location", $url);
}

/**
    Recargar la pagina, en false para cache, en true para cargar desde 0.
**/
function reload(){
    location.reload(false);
}

    
function tablas() {
    if( typeof ($.fn.DataTable) === 'undefined'){ return; }
    
    var tablas = ['tablaProductos'];

    for(i = 0; i < tablas.length; i++){
        var handleDataTableButtons = function() {
          if ($("#"+tablas[i]+"").length) {
            $("#"+tablas[i]+"").DataTable({
              dom: "Bfrtip",
              paging: true,
              buttons: [
                {
                    extend: "copy",
                    className: "btn-sm",
                    text: "Copiar"
                },
                {
                    extend: "csv",
                    className: "btn-sm",
                    text: "Descargar en CSV"
                },
                {
                    extend: "print",
                    className: "btn-sm",
                    text: "Imprimir Todo"
                },
                {
                    extend: "excel",
                    className: "btn-sm",
                    text: "Excel"
                },
                {
                    extend: "pdf",
                    className: "btn-sm",
                    text: "PDF"
                },
              ],
              "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
               },
               //order: [[0, "desc"]],
              responsive: true,
            });
          }
        };
        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();
        TableManageButtons.init();
    }
    selects();
}


/**
    Parametros de los selects options.
    @URL: https://silviomoreto.github.io/bootstrap-select/
**/
function selects(){
    $('.selectpicker').selectpicker({
        size: 9, 
        width: "fit", 
        title: "Seleccione una opción...",
        noneSelectedText: "Por favor, Seleccione uno.",
        liveSearch: true,
        liveSearchNormalize: true,
        liveSearchPlaceholder: "Buscar...",
    });
}

function notificacion($msg, $type){
    notif({
        type: $type,
        msg: $msg,
        position: "right",
        width: 200,
        height: 60,
        autohide: false,
        multiline: true,
        fade: true,
        bgcolor: "#0e3b5e",
        color: "#fff",
        opacity: 0.9,
    });
}

function back_to_top(){
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
}

function init(){
    back_to_top();
    tablas();
}